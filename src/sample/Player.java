package sample;

public class Player {
    private static Player instance;
    private String name;
    private int score;
    private int lives;

    //private constructor//
    private Player(){
        this.name=null;
        this.score=0;
        this.lives=3;
    };

   //to apply singleton//
    public static Player getInstance(){
        if ((instance)==null)
            return instance =new Player();
        else
            return instance;
    }

    //just setters and getters//
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }
}
