package sample;

public class Observer {

   Player player=Player.getInstance();

    private static Observer instance;
    private Observer(){}


    //to apply singleton//
    public static Observer getInstance(){
        if ((instance)==null)
            return instance =new Observer();
        else
            return instance;
    }


    public void updateScore(IGameObject object) {
        if (object.getObjectType().equals(ENUM.STRAWBERRY)) {
            player.setScore(player.getScore()+10);

        }
        if (object.getObjectType().equals(ENUM.KIWI)) {
            player.setScore(player.getScore()+10);
        }
        if (object.getObjectType().equals(ENUM.WATERMELON)) {
            player.setScore(player.getScore()+10);
        }

        if (object.getObjectType().equals(ENUM.SPECIAL_FRUIT_1)){
            player.setScore(player.getScore()+20);
        }
        if (object.getObjectType().equals(ENUM.SPECIAL_FRUIT_2)){
            player.setScore(player.getScore()+20);
        }
        else {
            player.setScore(player.getScore());
        }




    }

    public void updateLives(IGameObject gameObject) {
        if (gameObject.getObjectType().equals(ENUM.DANGEROUS_BOMB)){
            player.setLives(player.getLives()-1);
        }
        if (gameObject.getObjectType().equals(ENUM.FATAL_BOMB)){
            player.setLives(0);
        }


    }

    public void resetScore() {
        player.setScore(0);
    }

    public void resetLives() {
        player.setLives(3);
    }

    public void updatelivesifoffscreen(){
        player.setLives(player.getLives()-1);
    }

    public int displayScoreOnScreen() {
        int score;
        score=player.getScore();
        return score;
    }
    public int displayLivesOnScreen() {
        int lives ;
        lives=player.getLives();
        return lives;
    }

}


