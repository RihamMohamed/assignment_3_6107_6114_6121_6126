package sample;

public class Factory {

    public IGameObject create(int n){
        IGameObject iGameObject =null;


        //create
        if(n==0){
            iGameObject=new Strawberry();
        }
        else if (n==1){
            iGameObject=new Kiwi();
        }
        else if (n==2){
            iGameObject=new WaterMelon();
        }
        else if (n==3){
            iGameObject=new DangerousBomb();
        }
        else if (n==4){
            iGameObject=new FatalBomb();
        }
        else if (n==5){
            iGameObject=new SpecialBanana1();
        }
        else if (n==6){
            iGameObject=new SpecialBanana2();
        }
        else {
            return  null;
        }

        return iGameObject;
    }
}
