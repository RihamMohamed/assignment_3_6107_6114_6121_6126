package sample;

import javafx.animation.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Ellipse;
import javafx.util.Duration;

import java.util.Random;

public class Kiwi extends Object implements IGameObject{
    Image kiwiImage =new Image("file:Kiwi_Fruit.png");
    ImageView kiwiImageView = new ImageView(kiwiImage);

    int fallingVelocity = 5;
    int initialVelocity = 3;

    int width=1024;
    int height=683;
    Random random = new Random();


    @Override
    public ENUM getObjectType() {
        return ENUM.KIWI;
    }

    @Override
    public int getXlocation() {
        return random.nextInt(width);
    }

    @Override
    public int getYlocation() {
        return random.nextInt(height/2);
    }

    @Override
    public int getMaxHeight() {
        return random.nextInt((height/2)+100);
    }

    @Override
    public int getInitialVelocity() {
        return initialVelocity;
    }

    @Override
    public int getFallingVelocity() {
        return fallingVelocity;
    }

    @Override
    public Boolean isSliced() {
        return null;
    }

    @Override
    public Boolean hasMovedOffScreen() {
        return null;
    }

    @Override
    public void slice() {


    }

    @Override
    public void move(double time) {

        TranslateTransition translateTransition = new TranslateTransition();

        translateTransition.setDuration(Duration.millis(time));
        translateTransition.setNode(kiwiImageView);
        translateTransition.setFromY(683);
        translateTransition.setToY(getMaxHeight());
        translateTransition.setFromX(getXlocation());
        translateTransition.setToX(width/2);
        translateTransition.setAutoReverse(true);
        translateTransition.setCycleCount(2);
        translateTransition.setInterpolator(new Interpolator() {
            @Override
            protected double curve(double v) {
                return (v==1.0)? 1.0:1-Math.pow(2.0,-10*v);
            }
        });
        translateTransition.setOnFinished(e->{
            kiwiImageView.setVisible(false);
        });
        translateTransition.play();

    }

    @Override
    public ImageView[] getImages(){

        return null;
    }

    public ImageView appear(){

        kiwiImageView.setFitWidth(65);
        kiwiImageView.setFitHeight(65);

        move(1500);




        return kiwiImageView;


    }
}
