package sample;

import javafx.animation.*;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;


import javax.xml.bind.JAXBException;
import java.util.Collection;
import java.util.Random;

public class Level1 {

    ImageView image1;
    IGameObject gameObject;


    Player player=Player.getInstance();

    GameController gameController=new GameController();
    Observer observer=Observer.getInstance();





    //..........................................//





    public void level(VBox vBox, Label scoreLable, Label livesLable, String name, Stage window,Scene intro){




        Timeline timeline =new Timeline(new KeyFrame(
                Duration.seconds(5),
                event -> {

                    int noOfElements=getRandomNumberInRanges(7,9);

                    for (int i=1;i<=noOfElements;i++){


                        gameObject=gameController.createGameObject();


                        image1 = gameObject.appear();


                        vBox.getChildren().add(image1);




                        vBox.setOnDragDetected(e->{
                            vBox.startFullDrag();
                        });


                        ImageView finalImage=image1;
                        IGameObject finalgameObject=gameObject;

                        image1.setOnMouseDragEntered(e->{

                            if(observer.displayLivesOnScreen()==0){

                                gameOverDisplay(window,intro);


                            }
                            else {
                                finalImage.setVisible(false);
                                observer.updateScore(finalgameObject);
                                displayScore(scoreLable,observer.displayScoreOnScreen());
                                observer.updateLives(finalgameObject);
                                displayLives(livesLable,observer.displayLivesOnScreen());

                                System.out.println("..................");
                                System.out.println("killed");
                                System.out.println("Total score= "+player.getScore());
                                System.out.println("total Lives= "+player.getLives());


                                System.out.println("\nno of elements:"+noOfElements);


                                SavingScore savingScore = new SavingScore();
                                try {
                                    savingScore.toXml(name, observer.displayScoreOnScreen());
                                } catch (JAXBException exception) {
                                    exception.printStackTrace();
                                }

                            }




                        });




                    }



                }
        ));
        timeline.setCycleCount(5);
        timeline.play();








    }














        public static int getRandomNumberInRanges(int min, int max){
            if(min>=max){
                throw new IllegalArgumentException("max must be greater than min");
            }
            Random r =new Random();
            return r.nextInt((max-min)+1)+min;
        }

          public void displayScore(Label scoreLable,int value){
        scoreLable.setText("Score: "+value);

         }
         public void  displayLives(Label livesLable,int value){
        livesLable.setText("Lives: "+value);
         }

         public void gameOverDisplay(Stage window ,Scene intro) {

        Image gameOverImage =new Image("file:GameOver.jpg");
        ImageView gameOverImageView=new ImageView(gameOverImage);
             Button introReturnButton =new Button("Return");
             introReturnButton.setTranslateX(450);
             introReturnButton.setTranslateY(270);
             introReturnButton.setOnAction(e->{
                 window.setScene(intro);
             });

             StackPane gameOverstackPane = new StackPane(gameOverImageView,introReturnButton);

             Scene gameOverscene = new Scene(gameOverstackPane, 1024, 683);
             gameOverscene.getStylesheets().add("file:FruitNinja.css");
             window.setScene(gameOverscene);

         }





}








