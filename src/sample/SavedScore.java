package sample;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "savedScore")
@XmlType(propOrder = { "name", "score"})
@XmlAccessorType(XmlAccessType.FIELD)



public class SavedScore {


    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "score")
    private int score;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
