package sample;

import javafx.animation.TranslateTransition;
import javafx.scene.image.ImageView;
import java.io.IOException;

public interface IGameObject {
    /*
     *@return the type of game object
     */
    ENUM getObjectType();
    /*
     *@return X location of game object
     */
    int getXlocation();
    /*
     *@return Y location of game object
     */
    int getYlocation();
    /*
     *@return max Y location that the object can reach on the screen
     */
    int getMaxHeight();
    /*
     *@return velocity at which game object is thrown
     */
     int getInitialVelocity();
 /*
 *@return failing velocity of game object
 */
      int getFallingVelocity();
     /*
     *@return whether the object is sliced or not
     */
     Boolean isSliced();
    /*
     *@return whether the object is dropped off the screen or not
     */
     Boolean hasMovedOffScreen();
    /*
     *it is used to slice the object
     */
     void slice();
    /*
    *it is used to move the object on the screen
    @param deltaTime: time elapsed since the object is thrown
    it is used calculate the new position of
    fruit object.
    */
     void move(double time);
    /*
    *@return at least two images of the object, one when it is
    sliced and one when it is not.
    */
    public ImageView[] getImages() throws IOException;
    public ImageView appear();

}
