package sample;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "savedScores")
@XmlType(propOrder = { "savedScores"})
@XmlAccessorType(XmlAccessType.FIELD)

public class SavedScores {

    @XmlElement(name = "savedScore")
    private List<SavedScore> savedScores;

    public List<SavedScore> getSavedScores() {
        return savedScores;
    }

    public void setSavedScores(List<SavedScore> savedScores) {
        this.savedScores = savedScores;
    }



}
