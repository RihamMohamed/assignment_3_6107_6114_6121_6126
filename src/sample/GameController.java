package sample;

import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.util.Random;

public class GameController implements IGameActions{

    @Override
    public IGameObject createGameObject() {
        IGameObject gameObject;

        Factory factory=new Factory();

        Random random=new Random();
        int n =random.nextInt(7);

        gameObject = factory.create(n);

        System.out.println(n);
        return gameObject;
    }

    @Override
    public void updateObjectsLocations() {

    }

    @Override
    public void sliceObjects(VBox vBox) {



    }

    @Override
    public void saveGame() {

    }

    @Override
    public void loadGame() {

    }

    @Override
    public void ResetGame() {



    }

}
