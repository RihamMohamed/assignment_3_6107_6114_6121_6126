package sample;

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.xml.bind.JAXBException;


public class Main extends Application {



    @Override
    public void start(Stage window) throws Exception{

        Scene introScene,nameScene,rulesScene,playScene;
        Scene scoreBoardScene;
        Scene exitScene;
        //........................//

        Duration duration1=Duration.millis(1500);
        Duration duration2=Duration.millis(2500);
        Duration duration3=Duration.millis(3500);

        //.......................//
        StackPane introStackPane =new StackPane();
        VBox introVbox=new VBox();

        Image backgroundIntroImage=new Image("file:FruitNinga.png");
        ImageView backgroundIntroImageView =new ImageView(backgroundIntroImage);

        Image swordImage =new Image("file:sword.png");


        Label spacing =new Label("\n\n\n");
        Button startButton =new Button("Start");
        Button scoreBoardButton=new Button("ScoreBoard");
        Button exitButton=new Button("Exit");

        parallelTransition(startButton,duration1);
        parallelTransition(scoreBoardButton,duration2);
        parallelTransition(exitButton,duration3);



        introVbox.getChildren().addAll(spacing,startButton,scoreBoardButton,exitButton);
        introVbox.setSpacing(60);
        introVbox.setAlignment(Pos.CENTER);

        introStackPane.getChildren().addAll(backgroundIntroImageView,introVbox);

        introScene=new Scene(introStackPane,1024,683);
        introScene.getStylesheets().add("file:FruitNinja.css");
        introScene.setCursor(new ImageCursor(swordImage));

        //........................Game play............................//
        Image playImage=new Image("file:background2.jpg");
        ImageView playImageView=new ImageView(playImage);


        //.................//
        Label scoreLable =new Label("Score: 0");
        scoreLable.setTranslateX(450);
        scoreLable.setTranslateY(-300);
        Glow glow=new Glow(20);
        scoreLable.setEffect(glow);

        //................//
        Label livesLable =new Label("Lives: 3");
        livesLable.setTranslateX(450);
        livesLable.setTranslateY(-270);
        livesLable.setEffect(glow);
        //...................//
        Button introReturnButton =new Button("Return");
        parallelTransition(introReturnButton,duration1);
        introReturnButton.setTranslateX(-440);
        introReturnButton.setTranslateY(-276);
        introReturnButton.setEffect(glow);


        introReturnButton.setOnAction(e->{
            window.setScene(introScene);
        });



        VBox gamePlayVBox =new VBox();

        StackPane gamePlayStackPane=new StackPane();
        gamePlayStackPane.getChildren().addAll(playImageView,gamePlayVBox,scoreLable,livesLable,introReturnButton);

        Scene gamePlayScene =new Scene(gamePlayStackPane, 1024,683);
        gamePlayScene.getStylesheets().add("file:FruitNinja.css");
        gamePlayScene.setCursor(new ImageCursor(swordImage));



        //.......................................//

        Image nameImage=new Image("file:Name.jpg");
        ImageView nameImageView =new ImageView(nameImage);

        Label enterNameLabel =new Label("Enter your Name:");

        TextField nameTextfield =new TextField();
        nameTextfield.setPromptText("enter name here");

        Button startPlayButton =new Button("start Playing");
        Button rulesButton =new Button("Rules");
        Button returnButton1 =new Button("Return");

        parallelTransition(startPlayButton,duration2);
        parallelTransition(rulesButton,duration2);
        parallelTransition(returnButton1,duration2);




        StackPane nameStackpane =new StackPane();

        GridPane nameGridPane =new GridPane();
        nameGridPane.setPadding(new Insets(30,30,30,30));
        nameGridPane.setVgap(50);
        nameGridPane.setHgap(10);

        nameGridPane.setConstraints(enterNameLabel,0,0);
        nameGridPane.setConstraints(nameTextfield,1,0);
        nameGridPane.setConstraints(startPlayButton,0,1);
        nameGridPane.setConstraints(rulesButton,1,1);
        nameGridPane.setConstraints(returnButton1,2,1);

        nameGridPane.getChildren().addAll(enterNameLabel,nameTextfield,startPlayButton,rulesButton,returnButton1);
        nameGridPane.setAlignment(Pos.CENTER);
        nameGridPane.setTranslateX(140);

        nameStackpane.getChildren().addAll(nameImageView,nameGridPane);

        nameScene=new Scene(nameStackpane,1024,683);
        nameScene.getStylesheets().add("file:FruitNinja.css");
        nameScene.setCursor(new ImageCursor(swordImage));


        startPlayButton.setOnAction(e->{
            String name=nameTextfield.getText();
            window.setScene(gamePlayScene);

            Level1 level1=new Level1();
            level1.level(gamePlayVBox,scoreLable,livesLable,name,window,introScene);


        });

        returnButton1.setOnAction(e->{
            window.setScene(introScene);
        });
        //....................................//
        Image rulesImage =new Image("file:rules.jpg");
        ImageView rulesImageView =new ImageView(rulesImage);
        Button nameSceneReturnButton =new Button("Return");
        nameSceneReturnButton.setOnAction(e->{
            window.setScene(nameScene);
        });
        StackPane rulesStackPane =new StackPane();
        rulesStackPane.getChildren().addAll(rulesImageView,nameSceneReturnButton);
        rulesScene =new Scene(rulesStackPane,1024,683);
        rulesScene.getStylesheets().add("file:FruitNinja.css");
        rulesScene.setCursor(new ImageCursor(swordImage));

        rulesButton.setOnAction(e->{
            window.setScene(rulesScene);
        });

        //...........................//
        Image exitImage =new Image("file:exit1.jpg");
        ImageView exitImageView =new ImageView(exitImage);

        Label exitLable =new Label("Are You Sure You Want To Exit?");
        exitLable.setTranslateX(-34);
        Button yesButton =new Button("YES");
        yesButton.setTranslateX(40);
        Button noButton =new Button("NO");
        noButton.setTranslateX(-40);




        GridPane exitGridPane =new GridPane();
        exitGridPane.setPadding(new Insets(30,30,30,30));
        exitGridPane.setHgap(-100);
        exitGridPane.setVgap(50);

        exitGridPane.setConstraints(exitLable,0,0);
        exitGridPane.setConstraints(yesButton,0,1);
        exitGridPane.setConstraints(noButton,1,1);

        exitGridPane.getChildren().addAll(exitLable,yesButton,noButton);
        exitGridPane.setAlignment(Pos.CENTER);


        StackPane exitStackPane =new StackPane();
        exitStackPane.getChildren().addAll(exitImageView,exitGridPane);

        exitScene=new Scene(exitStackPane,1024,683);
        exitScene.getStylesheets().add("file:FruitNinja.css");
        exitScene.setCursor(new ImageCursor(swordImage));

        yesButton.setOnAction(e->{
            window.close();
        });
        noButton.setOnAction(e->{
            window.setScene(introScene);
        });

        //...........................................//

        Image scoreBoardImage =new Image("file:Score.jpg");
        ImageView scoreBoardImageView =new ImageView(scoreBoardImage);

        Label scoreIntroLable = new Label("SCOREBOARD");
        Label scoresLable =new Label();
        Button returnButton2 =new Button("Return");



        VBox scoreVbox =new VBox();
        scoreVbox.setSpacing(30);
        scoreVbox.setAlignment(Pos.CENTER);
        scoreVbox.getChildren().addAll(scoreIntroLable,scoresLable,returnButton2);

        StackPane scoreStackPane =new StackPane();
        scoreStackPane.getChildren().addAll(scoreBoardImageView,scoreVbox);


        scoreBoardScene =new Scene(scoreStackPane,1024,683);
        scoreBoardScene.getStylesheets().add("file:FruitNinja.css");
        scoreBoardScene.setCursor(new ImageCursor(swordImage));

        returnButton2.setOnAction(e->{
            window.setScene(introScene);
        });

        //.......................................//


        startButton.setOnAction(e->{
            window.setScene(nameScene);

        });
        scoreBoardButton.setOnAction(e->{
            window.setScene(scoreBoardScene);
            LoadingScoreBoard loadingScoreBoard=new LoadingScoreBoard();

            try {
                scoresLable.setText(loadingScoreBoard.loadScores());
            } catch (JAXBException exception) {
                exception.printStackTrace();
            }
            parallelTransition(returnButton2,duration2);
        });
        exitButton.setOnAction(e->{
            window.setScene(exitScene);
            parallelTransition(yesButton,duration2);
            parallelTransition(noButton,duration2);

        });





        window.setScene(introScene);
        window.setTitle("Fruit Ninja");
        window.show();
    }

    public void parallelTransition(Button button,Duration duration){
        ScaleTransition scaleTransition = new ScaleTransition();
        scaleTransition.setNode(button);
        scaleTransition.setDuration(duration);
        scaleTransition.setByX(1.25);
        scaleTransition.setByY(1.25);
        scaleTransition.setCycleCount(0);


        FadeTransition fadeTransition =new FadeTransition();
        fadeTransition.setNode(button);
        fadeTransition.setDuration(duration);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);

        ParallelTransition parallelTransition =new ParallelTransition(scaleTransition,fadeTransition);
        parallelTransition.play();
    }


    public static void main(String[] args) {
        launch(args);
    }
}




     /*
        //testing Player
        Player player1 =Player.getInstance();
        Player player2 =Player.getInstance();
        System.out.println(player1);
        System.out.println(player2);
        System.out.println(player1.getName());
        System.out.println(player1.getLives());
        System.out.println(player1.getScore());
        //...............................//

        */
