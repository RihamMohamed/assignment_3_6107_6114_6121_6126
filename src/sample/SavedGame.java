package sample;

import javax.xml.bind.annotation.*;

@XmlRootElement(name ="savedGame")
@XmlType(propOrder = { "savedScores" })
@XmlAccessorType(XmlAccessType.FIELD)



public class SavedGame {
    public SavedScores getSavedScores() {
        return savedScores;
    }

    public void setSavedScores(SavedScores savedScores) {
        this.savedScores = savedScores;
    }

    @XmlElement(name = "savedScores")
    private SavedScores savedScores = null;

}