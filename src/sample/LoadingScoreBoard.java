package sample;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class LoadingScoreBoard {
    public static String loadScores() throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(SavedGame.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        SavedGame savedGame = (SavedGame) unmarshaller.unmarshal(new File("saveScore.xml"));
        StringBuilder sb = new StringBuilder();
        for (SavedScore savedScore : savedGame.getSavedScores().getSavedScores()) {

            sb.append("Name: " + savedScore.getName() + "\t" + "Score: " + savedScore.getScore() + "\n");
        }
        return String.valueOf(sb);
    }


}
