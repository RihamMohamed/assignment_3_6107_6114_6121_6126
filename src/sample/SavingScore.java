package sample;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SavingScore {
    public static void toXml(String name, int score) throws JAXBException {

        JAXBContext jaxbContext1 = JAXBContext.newInstance(SavedGame.class);
        Unmarshaller unmarshaller=jaxbContext1.createUnmarshaller();
        SavedGame savedGame= (SavedGame) unmarshaller.unmarshal(new File("saveScore.xml"));
        List<SavedScore> oldList=new ArrayList<>();
        if(savedGame.getSavedScores().getSavedScores()!=null){
            oldList.addAll(savedGame.getSavedScores().getSavedScores());
        }

        SavedScores savedScores = new SavedScores();
        SavedScore savedScore = new SavedScore();
        savedScore.setName(name);
        savedScore.setScore(score);


        List<SavedScore> savedScoreList = new ArrayList<>(oldList);
        savedScoreList.add(savedScore);
        savedScores.setSavedScores(savedScoreList);
        savedGame.setSavedScores(savedScores);

        Marshaller marshaller = jaxbContext1.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(savedGame, new File("saveScore.xml"));

    }

}
