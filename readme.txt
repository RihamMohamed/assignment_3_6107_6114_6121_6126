Fruit Ninja - Game:

Alaa Shehab - 6107
Reem AbdelHaleem - 6114
Riham Mohamed - 6121
Salma Ahmed - 6126

MAIN POINTS IN PDF
1)Description:
It is a game of one level, when the player run it ,there will be 3 buttons in main menu: start playing, display the score (which is not working), exit. once he enters 
the start playing button, it will show the second theme where he/she can enter his name and there are 3 buttons: see the rules which show a pictures of game rules, 
play which starts playing scene, back which returnes the player to the first scene main menu. In playing scene, Random number of fruits are showed, everytime new 
group of fruit appear, so he/she has to split fruit to remove it, score and lives are shown on the scene while the game is on, each time he/she split a fruit, the
score increases.  If he/she split a bomb, then score decreases. if it is a fatal bomb, he will lose the game as the lives bacame zero.
2)Class Diagram:
3)Sequence Diagrams:
4)Design Patterns:
Singleton: Player Class.
Factory: Creation of Fruits.
Observer: used in changing the score of the player when he/she splits a fruit.
5)Snapshots of GUI:
6)User Guide:
ADDED
7)Problems Faced:
Glitching effect, some transparent picture of the fruits appeare durring running of the game.
We tried to use a while loopto make the images appear one after the other, but no images appeared.
We also tried different transitions(PathTransition, TranslateTransition, ParallelTransition, PauseTransition) but we had trouble 
with decrementing the lives.
Thus, we couldn't make more levels for the game.
8)Steps:
we spent a while researching for different motions possible: transition, rotation, path transition, fade transition, different kinds of transitions....
We tried to implement different types of those transition not only in gui of moving fruits but also in displaying gui buttons, etc...
we tried to make motion of fruits in different ways by translate transition, path transition by arch, SETTLED ON Timeline!
In trasition transition, we tried to "interpolater"<---- to change time of execution at first with small velocity "slow" then faster with bigger velocity in order to
show effect of gravity.
In order to execute different types of fruits each time, we used random twice, first time to show different group of fruits with eachother, second time to show in 
each group different kind of fruits which we don't expect.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-We used IntelliJIdea IDE to work on this program.
-JaveFX to work on the GUI.
-JAXB library to handle the xml files.
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------